FROM node:18-alpine3.17 as build
ARG configuration=production

WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY . /app
RUN npm run build --outputPath=dist --configuration=$configuration

FROM nginx:1.23.0-alpine
COPY --from=build /app/dist/angular-hello-world /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 4200

CMD ["nginx", "-g", "daemon off;"]
